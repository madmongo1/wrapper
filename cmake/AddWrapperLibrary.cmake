macro(AddWrapperLibrary AddWrapperLibrary_TARGET)
    set(AddWrapperLibrary_options)
    set(AddWrapperLibrary_oneValueArgs)
    set(AddWrapperLibrary_multiValueArgs)
    cmake_parse_arguments(AddWrapperLibrary "${AddWrapperLibrary_options}" "${AddWrapperLibrary_oneValueArgs}"
            "${AddWrapperLibrary_multiValueArgs}" ${ARGN})


endmacro()

macro(ConfigureInterfaceFiles)
    set(options)
    set(oneValueArgs)
    set(multiValueArgs FILES)
    cmake_parse_arguments(CIF "${options}" "${oneValueArgs}"
            "${multiValueArgs}" ${ARGN})

    foreach (CIF_file IN LISTS CIF_FILES)
        file(RELATIVE_PATH CIF_relfile "${PROJECT_SOURCE_DIR}" "${CIF_file}")
        string(REGEX REPLACE "\\.in$" "" CIF_out_file "${PROJECT_BINARY_DIR}/${CIF_relfile}")
        configure_file("${CIF_file}" "${CIF_out_file}" @ONLY)
        list(APPEND INTERFACE_FILES "${CIF_out_file}")
    endforeach ()
    set_source_files_properties(${INTERFACE_FILES_IN} PROPERTIES HEADER_FILE_ONLY 1)

endmacro()

macro(ConfigureSourceFiles)
    set(options)
    set(oneValueArgs)
    set(multiValueArgs FILES)
    cmake_parse_arguments(CSF "${options}" "${oneValueArgs}"
            "${multiValueArgs}" ${ARGN})

    foreach (CSF_file IN LISTS CSF_FILES)
        file(RELATIVE_PATH CSF_relfile "${PROJECT_SOURCE_DIR}" "${CSF_file}")
        string(REGEX REPLACE "\\.in$" "" CSF_out_file "${PROJECT_BINARY_DIR}/${CSF_relfile}")
        configure_file("${CSF_file}" "${CSF_out_file}" @ONLY)
        list(APPEND INTERFACE_FILES "${CSF_out_file}")
    endforeach ()
    set_source_files_properties(${INTERFACE_FILES_IN} PROPERTIES HEADER_FILE_ONLY 1)

endmacro()

function(CString)
    set(options)
    set(oneValueArgs EXTENT OUTVAR)
    set(multiValueArgs)
    cmake_parse_arguments("CSL" "${options}" "${oneValueArgs}"
            "${multiValueArgs}" ${ARGN})

    if (NOT CSL_OUTVAR)
        message(FATAL_ERROR "[AddWrapperLibrary::CString] OUTVAR not specified")
    endif ()

    if (DEFINED CSL_EXTENT)
        string(LENGTH "${CSL_EXTENT}" CSL_STRING_LENGTH)
        math(EXPR CSL_STRING_EXTENT "${CSL_STRING_LENGTH} + 1")
        set(${CSL_OUTVAR} "${CSL_STRING_EXTENT}" PARENT_SCOPE)
    else ()
        message(FATAL_ERROR "[AddWrapperLibrary::CString] No command specified. Valid commands are: EXTENT - ${CSL_EXTENT}")
        message(FATAL_ERROR "[AddWrapperLibrary::CString] No command specified. Valid commands are: EXTENT")
    endif ()

endfunction()

macro(AddStandardIncludes ASI_target)
    target_include_directories(${ASI_target} SYSTEM PUBLIC $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/include>)
    target_include_directories(${ASI_target} SYSTEM PUBLIC $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>)
    target_include_directories(${ASI_target} SYSTEM PUBLIC $<INSTALL_INTERFACE:${PROJECT_BINARY_DIR}/include>)
    target_include_directories(${ASI_target} PRIVATE $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/src>)
    target_include_directories(${ASI_target} PRIVATE $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/src>)
endmacro()
