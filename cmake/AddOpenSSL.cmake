macro(AddOpenSSL AddOpenSSL_TARGET)
    set(AddOpenSSL_options)
    set(AddOpenSSL_oneValueArgs)
    set(AddOpenSSL_multiValueArgs)
    cmake_parse_arguments(AddOpenSSL "${AddOpenSSL_options}" "${AddOpenSSL_oneValueArgs}"
            "${AddOpenSSL_multiValueArgs}" ${ARGN})

    hunter_add_package(OpenSSL)
    find_package(OpenSSL)

    set(AddOpenSSL_libs OpenSSL::SSL OpenSSL::Crypto)

    target_link_libraries(${AddOpenSSL_TARGET} PUBLIC ${AddOpenSSL_libs})

endmacro()
