project(WrapperOpenSSL)
set(libname "wrapper-openssl")
set(exported_name "Wrapper::OpenSSL")


set(SRC_FILES)
set(INTERFACE_FILES)
set(INTERFACE_FILES_IN)
set(TEST_SRC)

sugar_include(include)
sugar_include(src)
sugar_include(test)

set_source_files_properties(${INTERFACE_FILES_IN} PROPERTIES HEADER_FILE_ONLY 1)

ConfigureInterfaceFiles(FILES ${INTERFACE_IN_FILES})


add_library(${libname} ${SRC_FILES} ${INTERFACE_FILES} ${INTERFACE_FILES_IN})
target_link_libraries(${libname} PUBLIC Wrapper::Core)
find_package(Threads)
target_link_libraries(${libname} PUBLIC Threads::Threads)
AddStandardIncludes(${libname})
include(AddOpenSSL)
AddOpenSSL(wrapper-openssl)
SetCompileOptions("${libname}")

add_library(${exported_name} ALIAS ${libname})

set(WRAPPER_TEST_SRC ${WRAPPER_TEST_SRC} ${TEST_SRC} PARENT_SCOPE)
set(WRAPPER_ALL_LIBRARIES ${WRAPPER_ALL_LIBRARIES} ${exported_name} PARENT_SCOPE)
