//
// Created by Richard Hodges on 20/07/2018.
//

#pragma once

namespace wrapper { namespace openssl {

    struct openssl_service_init
    {
        openssl_service_init();
    };

    static openssl_service_init service_initialiser;
}}
