//
// Created by Richard Hodges on 18/07/2018.
//

#pragma once

#include <wrapper/openssl/config.hpp>
#include <wrapper/openssl/big_num_ptr.hpp>

namespace wrapper { namespace openssl {

    struct big_num_ops
    {
        using implementation_type = big_num_ptr;


        int compare(implementation_type const& l, implementation_type const& r)
        {
            assert(!l.empty());
            assert(!r.empty());
            return BN_cmp(l.get(), r.get());
        }

    };

    struct big_num_service : big_num_ops
    {

        void set_random_range(implementation_type& target, implementation_type const& source)
        {
            if (target.empty())
                target = construct();

            if (!BN_rand_range(target.get(), source.get()))
                throw big_num_failure("big_num_service::rand_range - BN_rand_range");
        }

        implementation_type construct()
        {
            auto result = implementation_type(BN_new());
            if (result.empty())
                throw big_num_failure("big_num_service::construct - BN_new()");
            return result;
        }

        implementation_type construct(const char *str)
        {
            auto impl = implementation_type();
            auto len  = BN_dec2bn(impl.address(), str);
            if (len == 0) {
                while(auto e = ERR_get_error())
                {
                    std::cout << "error: " << e << std::endl;
                }
                throw big_num_failure("big_num_service::construct - BN_dec2bn()");
            }
            return impl;
        }


        /**
         * Set target to a random value between 0 and source.
         * @param target
         * @param source
         */

    };

}}
