//
// Created by Richard Hodges on 18/07/2018.
//

#pragma once

#include <wrapper/core/c_string_ops.hpp>
#include <wrapper/core/adopted_pointer.hpp>
#include <wrapper/core/choose_impl_storage.hpp>
#include <wrapper/openssl/config.hpp>
#include <openssl/crypto.h>
#include <cstring>
#include <memory>

namespace wrapper { namespace openssl {

    /// Provide implementation services for a returned_string.
    /// This class is a model of a Service concept. @see wrapper::core::Service
    /// @see returned_string
    struct returned_string_service
        : ::wrapper::core::c_string_ops<char>
    {
        /// The type of character stored in the string
        using char_type = typename c_string_ops<char>::char_type;

        /// The type of character exposed in const strings
        using const_char_type = typename c_string_ops<char>::const_char_type;

        /// The type of implementation
        using implementation_type = typename c_string_ops<char>::implementation_type;

        static constexpr auto acquire_service() -> returned_string_service
        {
            return {};
        }


        /// Construct an implementation from an adopted string.
        /// @param impl is a reference to the implementation, which should not have been previously assigned
        /// @param ptr is an adopted_pointer wrapping a char* which has been returned by the OPENSSL library
        /// @exception noexcept
        void construct(implementation_type& impl, ::wrapper::core::adopted_pointer<char_type> ptr) noexcept
        {
            assert(invalid(impl));
            impl = std::move(ptr).get();
        }

        /// Destroy an implementation.
        /// @param impl is a reference to the implementation, which shall be either nullptr or the address of a string
        ///             returned by the openssl library
        /// @exception noexcept
        auto destroy(implementation_type& impl) noexcept
        {
            assert(!invalid(impl));
            OPENSSL_free(impl);
            impl = invalid();
        }

        // implementated in base clasee
        // void move(implementation_type& dest, implementation_type& source) noexcept

        // not implemented - therefore object is not copyable
        // void clone(implementation_type& dest, implementation_type const& source) noexcept

    };


    struct returned_string_base
        : wrapper::core::choose_impl_storage<returned_string_service>
    {
        using impl_storage_type = wrapper::core::choose_impl_storage<returned_string_service>;
        using service_type = returned_string_service;
        using implementation_type = typename service_type::implementation_type;
        using char_type = typename service_type::char_type;
        using const_char_type = std::add_const_t<char_type>;

        using impl_storage_type::impl_storage_type;
    };

    /// An object which represents a string returned by the openssl library
    struct returned_string
        : wrapper::core::basic_c_string_object_interface<returned_string_base>
    {

        /// Adopt a string returned by openssl
        returned_string(wrapper::core::adopted_pointer<char> ptr)
            : wrapper::core::basic_c_string_object_interface<returned_string_base>(std::make_tuple(), std::make_tuple(std::move(ptr)))
        {

        }

        bool empty() const
        {
            return !bool(impl());
        }

        /// Return a pointer to the wrapped c-style string
        /// @pre !empty()
        /// @returns a pointer to the c-style-string
        const_char_type *c_str() const
        {
            return service().data(impl());
        }

        /// Return the size of the string
        /// @returns std::size_t containing the number of characters of the string.
        std::size_t size() const
        {
            return service().size(impl());
        }

    };

    inline auto operator <<(std::ostream& os, returned_string const& str) -> std::ostream&
    {
        if (str.empty())
        {
            return os << "{null}";
        }
        else
        {
            return os << str.c_str();
        }
    }


}}