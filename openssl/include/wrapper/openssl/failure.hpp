//
// Created by Richard Hodges on 20/07/2018.
//

#pragma once

#include <wrapper/openssl/config.hpp>
#include <openssl/err.h>

#include <stdexcept>
#include <string>

namespace wrapper { namespace openssl {

    struct failure
        : std::runtime_error
    {
        failure(std::string const& message)
            : runtime_error(message + ";"+ build_ssl_error_message())
        {
        }


    private:
        static auto build_ssl_error_message() -> std::string;
    };


}}