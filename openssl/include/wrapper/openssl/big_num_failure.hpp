//
// Created by Richard Hodges on 18/07/2018.
//

#pragma once

#include <wrapper/openssl/config.hpp>
#include <wrapper/openssl/failure.hpp>

#include <stdexcept>

namespace wrapper { namespace openssl {

    /// Indicates a failure in the creation or manipulation of a big_num
    struct big_num_failure
        : failure
    {
        using failure::failure;
    };
}}