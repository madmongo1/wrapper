//
// Created by Richard Hodges on 18/07/2018.
//

#pragma once

#include <wrapper/openssl/config.hpp>
#include <wrapper/openssl/big_num_failure.hpp>
#include <openssl/bn.h>
#include <utility>

namespace wrapper { namespace openssl {

    struct big_num_ptr
    {
        using element_type = BIGNUM;
        using pointer = BIGNUM *;
        using const_pointer = BIGNUM const *;

        big_num_ptr()
            : ptr_(nullptr) {}

        // take ownership
        big_num_ptr(pointer ptr)
            : ptr_(ptr) {}

        // move
        big_num_ptr(big_num_ptr&& other) noexcept
            : ptr_(nullptr)
        {
            std::swap(ptr_, other.ptr_);
        }

        big_num_ptr& operator =(big_num_ptr&& other) noexcept
        {
            return swap(big_num_ptr(std::move(other)));
        }

        // copy
        big_num_ptr(big_num_ptr const& other)
            : ptr_(clone(other.ptr_))
        {

        }

        // copy
        big_num_ptr& operator =(big_num_ptr const& other)
        {
            return swap(big_num_ptr(other));
        }

        ~big_num_ptr() noexcept
        {
            if (ptr_)
                BN_clear_free(ptr_);
        }

        auto swap(big_num_ptr& other) noexcept -> big_num_ptr&
        {
            std::swap(ptr_, other.ptr_);
            return *this;
        }

        auto swap(big_num_ptr&& other) noexcept -> big_num_ptr&
        {
            return swap(other);
        }

        pointer *address()
        {
            return std::addressof(ptr_);
        }

        pointer get()
        {
            return ptr_;
        }

        const_pointer get() const
        {
            return ptr_;
        }

        bool empty() const
        {
            return !ptr_;
        }

    private:

        static pointer clone(const_pointer source)
        {
            pointer result = nullptr;
            if (source)
            {
                result = BN_dup(source);
                if (!result)
                    throw big_num_failure("BigNumPointer::clone - BN_dup()");
            }
            return result;
        }

        BIGNUM *ptr_;
    };

}}
