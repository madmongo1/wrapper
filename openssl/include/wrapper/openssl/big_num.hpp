//
// Created by Richard Hodges on 18/07/2018.
//

#pragma once

#include <wrapper/openssl/config.hpp>
#include <wrapper/openssl/big_num_failure.hpp>
#include <openssl/bn.h>
#include <stdexcept>
#include <wrapper/openssl/big_num_service.hpp>
#include <wrapper/openssl/returned_string.hpp>
#include <wrapper/adopt.hpp>

namespace wrapper { namespace openssl {

    struct decimal_flag
    {
    };

    struct hex_flag
    {
    };

    static constexpr auto decimal = decimal_flag{};

    static constexpr auto hex     = hex_flag{};


    /// Provides a value-semantic handle to an openssl BIGNUM.
    /// Allows BIGNUM objects to be used as value objects in c++ expressions
    struct big_num
    {
        using service_type = big_num_service;
        using implementation_type = service_type::implementation_type;

        /// Construct an empty big_num.
        big_num()
            : bn_(service().construct())
        {
        }

        /// Construct a big_num from an ascii representation of a decimal value.
        /// @exception big_num_failure if an error occurs
        big_num(const char *str)
            : bn_(service().construct(str))
        {
        }

        /// Move constructor
        big_num(implementation_type&& impl)
            : bn_(std::move(impl))
        {
        }

        /// Swap two big_num objects
        void swap(big_num& other) noexcept
        {
            std::swap(bn_, other.bn_);
        }

        /// Return a new big_num with a value between 0 and the value of this instance.
        /// @returns big_num
        auto get_random_range() const -> big_num
        {
            auto result = service().construct();
            service().set_random_range(result, impl());
            return big_num{std::move(result)};
        }


        /// Return the value of this object as a decimal string.
        /// @returns returned_string representing the decimal value.
        auto as_decimal_string() const -> returned_string
        {
            return returned_string(wrapper::adopt(BN_bn2dec(impl().get())));
        }

        /// Return the value of this object as a hex string.
        /// @returns returned_string representing the hex value.
        auto as_hex_string() const -> returned_string
        {
            return returned_string(wrapper::adopt((BN_bn2hex(impl().get()))));
        }

        /// Return the value of this object as a vector of binary bytes - big-endian.
        /// @returns std::vector<std::uint8_t>
        auto as_bytes() const -> std::vector <std::uint8_t>
        {
            auto bytes  = BN_num_bytes(bn_.get());
            auto result = std::vector<std::uint8_t>(bytes);
            BN_bn2bin(bn_.get(), result.data());
            return result;
        }

        bool operator==(big_num const& r) const
        {
            return service().compare(impl(), r.impl()) < 0;
        }

    private:
        constexpr service_type service() const
        {
            return service_type();
        }

        constexpr implementation_type& impl()
        {
            return bn_;
        }

        constexpr implementation_type const& impl() const
        {
            return bn_;
        }

        implementation_type bn_;
    };

    inline void swap(big_num& l, big_num& r)
    {
        l.swap(r);
    }

    inline auto rand_range(big_num const& src) -> big_num
    {
        return src.get_random_range();
    }

    inline auto operator<<(std::ostream& os, big_num const& bn) -> std::ostream&
    {
        if(os.flags() & std::ios::hex)
        {
            return os << bn.as_hex_string();
        }
        else
        {
            return os << bn.as_decimal_string();
        }
    }

    auto to_string(big_num const& r) -> returned_string
    {
        return r.as_decimal_string();
    }


}}