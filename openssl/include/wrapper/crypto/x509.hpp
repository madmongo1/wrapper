//
// Created by Richard Hodges on 24/07/2018.
//

#pragma once

#include <wrapper/core/base_impl_storage.hpp>
#include <wrapper/crypto/x509_service.hpp>

namespace wrapper::crypto
{
    struct x509
    : wrapper::core::base_impl_storage<x509_service>
    {
        using impl_storage = wrapper::core::base_impl_storage<x509_service>;
        using service_type = impl_storage::service_type;

        x509() : impl_storage(std::make_tuple(), std::make_tuple())
        {

        }


    };
}