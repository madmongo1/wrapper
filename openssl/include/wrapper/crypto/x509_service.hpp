//
// Created by Richard Hodges on 24/07/2018.
//

#pragma once

#include <openssl/x509.h>
#include <wrapper/openssl/failure.hpp>
#include <cassert>
#include <wrapper/core/adopted_pointer.hpp>

namespace wrapper { namespace crypto
{
    struct x509_service
    {
        using implementation_type = X509 *;

        static constexpr auto acquire_service() -> x509_service
        {
            return {};
        }

        static constexpr auto invalid() -> implementation_type
        {
            return nullptr;
        }

        static constexpr auto invalid(implementation_type impl) -> bool
        {
            return impl = invalid();
        }

        static auto construct(implementation_type &impl, wrapper::core::adopted_pointer<X509> p)
        {
            impl = std::move(p).get();
        }

        static auto construct(implementation_type &impl) -> void
        {
            impl = X509_new();
            if (invalid(impl))
            {
                throw openssl::failure("x509_service::construct - X509_new()");
            }
        }

        static auto destroy(implementation_type &impl) noexcept -> void
        {
            assert(!invalid(impl));
            X509_free(impl);
            impl = invalid();
        }

        static auto move(implementation_type &to, implementation_type &from) noexcept -> void
        {
            assert(invalid(to));
            to = from;
            from = invalid();
        }

        static auto clone(implementation_type &to, implementation_type const &from) -> void
        {
            assert(invalid(to));
            to = X509_dup(from);
            if (invalid(to))
            {
                throw openssl::failure("x509_service::clone - X509_dup()");
            }
        }
    };
}}