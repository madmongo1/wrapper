//
// Created by Richard Hodges on 24/07/2018.
//

#pragma once

#include <openssl/ossl_typ.h>

namespace wrapper { namespace crypto { namespace asn1 {

    struct time_service
    {
        using implementation_type = ASN1_TIME*;

        static constexpr auto acquire_service() { return time_service(); }

        auto construct(implementation_type& impl)
        {

        }

    };

}}}