//
// Created by Richard Hodges on 20/07/2018.
//


#include <gtest/gtest.h>
#include <wrapper/openssl/big_num.hpp>

TEST(big_num, basics)
{
    auto b = wrapper::openssl::big_num();

    try
    {
        b = "sdfghjk";
    }
    catch(wrapper::openssl::big_num_failure& e)
    {
        EXPECT_STREQ("big_num_service::construct - BN_dec2bn();no error reported from openssl", e.what());
    }
    catch(std::exception& e)
    {
        FAIL() << e.what();
    }

    EXPECT_NO_THROW(b = "1234567890");
    EXPECT_EQ("1234567890", to_string(b));
}
