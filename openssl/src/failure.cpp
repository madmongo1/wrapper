//
// Created by Richard Hodges on 20/07/2018.
//

#include <wrapper/openssl/failure.hpp>
#include <openssl/err.h>

namespace wrapper { namespace openssl {

    auto failure::build_ssl_error_message() -> std::string
    {
        auto result = std::string();
        const char* sep = "";

        auto err = ERR_get_error();
        if (!err)
        {
            result = "no error reported from openssl";
        }
        else
        {
            do
            {
                constexpr auto buflen = 256;
                char buf[buflen];
                ERR_error_string_n(err, buf, buflen);
                result += sep;
                result += buf;
                sep = ";";
            }
            while((err = ERR_get_error()) != 0);
        }

        return result;
    }
}}