//
// Created by Richard Hodges on 20/07/2018.
//


#include <wrapper/openssl/openssl_service.hpp>
#include <openssl/ssl.h>
#include <memory>
#include <iostream>

namespace wrapper { namespace openssl {

    static int service_count;

    openssl_service_init::openssl_service_init()
    {
        if (service_count++ == 0)
        {
#if OPENSSL_VERSION_NUMBER < 0x10100000L
            SSL_library_init();
#else
            OPENSSL_init_ssl(OPENSSL_INIT_SSL_DEFAULT, NULL);
#endif
        }
    }

}}