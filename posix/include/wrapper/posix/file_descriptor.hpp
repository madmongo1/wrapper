//
// Created by Richard Hodges on 13/08/2018.
//

#pragma once

#include <wrapper/posix/file_descriptor_service.hpp>
#include <wrapper/core/base_impl_storage.hpp>
#include <wrapper/core/filesystem.hpp>
#include <boost/format.hpp>

namespace wrapper::posix {

    template<class Service>
    struct native_handle_base
    {
        using service_type = Service;
        using native_handle_type = typename service_type::native_handle_type;

        constexpr native_handle_base(native_handle_type nh)
            : handle_(nh)
        {

        }

        constexpr auto service() const -> service_type
        {
            return service_type();
        }

        auto handle() const -> native_handle_type const&
        {
            return handle_;
        }

        auto handle() -> native_handle_type&
        {
            return handle_;
        }

        auto swap(native_handle_base& other) noexcept
        {
            std::swap(handle_, other.handle_);
        }

        auto release() noexcept -> native_handle_type
        {
            auto result = handle_;
            handle_ = service().invalid_handle_value();
            return result;
        }


    private:
        native_handle_type handle_;
    };

    template<class Service>
    struct native_handle_reference
        : native_handle_base<Service>
    {
        using base_class = native_handle_base<Service>;
        using service_type = typename base_class::service_type;
        using native_handle_type  = typename base_class::native_handle_type;

        using base_class::base_class;
    };

    template<class Service>
    struct native_handle_unique
        : native_handle_base<Service>
    {
        using base_class = native_handle_base<Service>;
        using service_type = typename base_class::service_type;
        using native_handle_type  = typename base_class::native_handle_type;

        using base_class::base_class;
        using base_class::handle;
        using base_class::service;
        using base_class::swap;

        native_handle_unique(native_handle_unique&& other) noexcept
            : base_class(other.release())
        {
        }

        native_handle_unique& operator =(native_handle_unique&& other)
        {
            auto tmp = std::move(other);
            swap(tmp);
        }

        ~native_handle_unique()
        {
            if (auto fd = handle(); service().valid(fd))
            {
                service().destroy(fd);
            }
        }

    };

    template<class Owner>
    struct file_descriptor_interface
    {
        auto stat() const -> file_status
        try
        {
            auto self    = get_self();
            auto&& service = self->service();
            auto&& handle = self->handle();
            auto result = file_status();
            service.stat(handle, result);
            return result;
        }
        catch (...)
        {
            auto fmt = boost::format("file_descriptor::stat(%1%) - failed") % get_self()->handle();
            std::throw_with_nested(std::runtime_error(fmt.str()));
        }

        auto tell() const -> std::streamoff
        try
        {
            auto self    = get_self();
            auto&& service = self->service();
            auto&& handle = self->handle();
            return service.tell(handle);
        }
        catch(...)
        {
            auto fmt = boost::format("file_descriptor::tell(%1%) - failed") % get_self()->handle();
            std::throw_with_nested(std::runtime_error(fmt.str()));
        }

        bool valid() const
        {
            auto self    = get_self();
            auto&& service = self->service();
            auto&& handle = self->handle();
            return service.valid(handle);
        }

        auto get_self() -> Owner * { return static_cast<Owner *>(this); }

        auto get_self() const -> Owner const * { return static_cast<Owner const *>(this); }

        friend auto operator<<(std::ostream& os, file_descriptor_interface const& fd)
        {
            return os << "{ handle : " << fd.handle() << " }";
        }
    };

    struct file_descriptor_reference
        : native_handle_reference<file_descriptor_service>,
          file_descriptor_interface<file_descriptor_reference>
    {

        using base_class = native_handle_reference<file_descriptor_service>;
        using base_class::base_class;

        constexpr static auto stdin() -> file_descriptor_reference
        {
            return file_descriptor_reference(0);
        }

        constexpr static auto stdout() -> file_descriptor_reference
        {
            return file_descriptor_reference(1);
        }

        constexpr static auto stderr() -> file_descriptor_reference
        {
            return file_descriptor_reference(2);
        }
    };


    struct file_descriptor
        : native_handle_unique<file_descriptor_service>,
          file_descriptor_interface<file_descriptor>
    {
        using base_class = native_handle_unique<file_descriptor_service>;
        using base_class::base_class;

        static auto adopt(native_handle_type fd) -> file_descriptor
        {
            return file_descriptor(fd);
        }

        operator file_descriptor_reference() const
        {
            return file_descriptor_reference(handle());
        }

    private:
        file_descriptor(native_handle_type fd)
            : base_class(fd) {}

    };

    auto dup(native_handle_base<file_descriptor_service> source) -> file_descriptor;

    auto open(filesystem::path const& where, std::ios::openmode = std::ios::in) -> file_descriptor;


    inline
    auto stat(file_descriptor_reference fd) -> file_status
    {
        return fd.stat();
    }

    inline
    auto size(file_descriptor_reference fd) -> std::size_t
    {
        return stat(fd).size();
    }

    inline auto tell(file_descriptor_reference fd) -> std::streamoff
    {
        return fd.tell();
    }

    /// Consume the remaining bytes of the file into a string.
    auto gobble(file_descriptor_reference fd) -> std::string;

    /// Write data to a file or throw an exception.
    auto write(file_descriptor_reference fd, const void* buffer, std::size_t length) -> std::size_t;

}