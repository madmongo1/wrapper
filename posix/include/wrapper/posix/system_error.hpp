//
// Created by Richard Hodges on 23/09/2018.
//

#pragma once

#include <wrapper/core/system_error.hpp>
#include <cassert>
#include <wrapper/core/variant.hpp>

namespace wrapper::posix {

    namespace system = wrapper::core::system;

    struct fixed_size_buffer
    {
        using storage_type = std::array<char, 24>;

        char *data()
        {
            return data_.data();
        }

        char const *data() const
        {
            return data_.data();
        }

        static constexpr std::size_t capacity()
        {
            return std::extent_v<storage_type> -1;
        }

        storage_type data_ = {};
    };

    struct variable_size_buffer
    {
        using tuple_representation = std::tuple<char *, std::size_t>;

        variable_size_buffer()
            : data_(nullptr)
            , allocated_bytes_(0) {}

        variable_size_buffer(variable_size_buffer&& other) noexcept
            : variable_size_buffer(other.release())
        {
        }

        ~variable_size_buffer()
        {
            if (data_)
                free(data_);
        }

        auto release() noexcept -> tuple_representation
        {
            auto result = tuple_representation(data_, allocated_bytes_);
            data_            = nullptr;
            allocated_bytes_ = 0;
            return result;
        }

        char *data()
        {
            return data_;
        }

        char const *data() const
        {
            return data_;
        }

        auto capacity() const -> std::size_t
        {
            return allocated_bytes_ ? allocated_bytes_ - 1 : 0;
        }

    private:

        variable_size_buffer(tuple_representation args)
            : data_(std::get<char *>(args))
            , allocated_bytes_(std::get<std::size_t>(args)) {}

        char *data_;
        std::size_t allocated_bytes_;
    };

    struct os_string
    {
        using storage_type = core::variant<fixed_size_buffer, variable_size_buffer>;

        auto data() -> char *
        {
            auto f = [](auto&& arg) { return arg.data(); };
            return core::visit(f, storage_);
        }

        auto capacity() const -> std::size_t
        {
            auto f = [](auto&& arg) { return arg.capacity(); };
            return core::visit(f, storage_);
        }

        storage_type storage_;
    };

    using error_code = boost::system::error_code;
    using system_error = boost::system::system_error;

    [[noreturn]]
    static bool throw_errno()
    {
        auto error_number = errno;
        assert(error_number != 0);


    }

    [[noreturn]]
    static bool throw_errno(const char *context)
    {

    }
}