//
// Created by Richard Hodges on 25/09/2018.
//

#pragma once
#include <wrapper/core/cstring_view.hpp>
#include <cassert>
#include <cstdio>

namespace wrapper::posix
{
    inline
    auto getenv(const char* pvar) -> core::cstring_view
    {
        assert(pvar);
        return core::cstring_view(std::getenv(pvar));
    }
}