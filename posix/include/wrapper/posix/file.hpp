//
// Created by Richard Hodges on 13/08/2018.
//

#pragma once

#include <wrapper/posix/file_service.hpp>

#include <cstdio>
#include <wrapper/core/choose_impl_storage.hpp>
#include <system_error>
#include <cassert>
#include <unistd.h>
#include <stdio.h>
#include <string>

namespace wrapper::posix
{

    template<class Handle>
    struct file_interface
    {
        void write(const char *data, std::size_t bytes)
        {
            auto my = self();
            my->service().write(my->native_handle(), data, bytes);
        }

    private:
        Handle *self()
        {
            return static_cast<Handle *>(this);
        }
    };


    struct file : core::choose_impl_storage<file_service>, file_interface<file>
    {
        file(const char *path) : core::choose_impl_storage<file_service>(std::make_tuple(), std::make_tuple(path))
        {

        }


    };

}