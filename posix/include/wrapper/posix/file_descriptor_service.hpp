//
// Created by Richard Hodges on 01/09/2018.
//

#pragma once
#include <wrapper/posix/filesystem.hpp>
#include <wrapper/posix/system_error.hpp>
#include <wrapper/posix/handles_errno.hpp>
#include <sys/stat.h>

namespace wrapper::posix
{
    struct file_descriptor_service
        : handles_errno
    {
        //
        // meet requirements of a service
        //

        using native_handle_type = int;

        static constexpr bool valid(native_handle_type impl) noexcept
        {
            return impl != invalid_handle_value();
        }

        static constexpr auto invalid_handle_value() noexcept -> native_handle_type
        {
            return native_handle_type(-1);
        }

        void destroy(native_handle_type impl) noexcept;

        native_handle_type clone(native_handle_type source);

        static constexpr auto acquire_service() { return file_descriptor_service(); }

        //
        // interface
        //

        static
        auto open(filesystem::path const& where, std::ios::openmode mode) -> native_handle_type;

        constexpr static auto invalid(native_handle_type handle) -> bool
        {
            return handle == invalid_handle_value();
        }

        void stat(native_handle_type handle, struct stat& target);

        std::streampos tell(native_handle_type handle)
        {
            auto result = ::lseek(handle, 0, SEEK_CUR);
            if (result == ::off_t(-1))
                throw_errno(errno);
            return result;
        }
    };

    struct file_status : ::stat
    {
        std::streamoff size() const { return this->st_size; }
        operator struct ::stat&()
        {
            return *this;
        }
    };

}