//
// Created by Richard Hodges on 01/09/2018.
//

#pragma once

#include <wrapper/posix/file_descriptor_service.hpp>
#include <cstdio>
#include <string>

namespace wrapper::posix {

    struct file_impl
    {
        std::FILE* fp_ = nullptr;
        std::string mode_;
    };

    struct file_service
    {
        using implementation_type = file_impl;

        constexpr file_service()
        {

        }

        static constexpr bool invalid(implementation_type const& impl)
        {
            return impl.fp_ == nullptr;
        }

        static auto invalid() -> implementation_type
        {
            return file_impl { nullptr, std::string() };
        }

        void construct(implementation_type& impl, const char *path, std::string mode = "r");

        void destroy(implementation_type& impl);

        void clone(implementation_type& target, implementation_type const& source);

        static constexpr auto acquire_service() { return file_service(); }

    private:

        static auto fileno(std::FILE* fp) -> int;
        static auto dup(int) -> int;
        static auto fdopen(int, const char* mode) -> std::FILE*;

        static int validate_fd(int);
        static std::FILE* validate_fp(std::FILE*);

        [[noreturn]]
        static void throw_system_error(int eno);
    };

}
