//
// Created by Richard Hodges on 28/09/2018.
//

#pragma once

#include <wrapper/posix/file_descriptor.hpp>
#include <string>
#include <wrapper/posix/filesystem.hpp>

namespace wrapper::posix
{
    struct temporary_file_descriptor
    {
        temporary_file_descriptor(file_descriptor &&fd, filesystem::path &&path_name)
        : fd_(std::move(fd)), path_name_(std::move(path_name)) {}

        ~temporary_file_descriptor()
        {
            if (valid())
            {
                ::unlink(path_name_.c_str());
            }
        }

        bool valid() const
        {
            return fd_.valid();
        }

        auto release() -> file_descriptor
        {
            return std::move(fd_);
        }

        auto fd() const -> file_descriptor_reference
        {
            return file_descriptor_reference(fd_);
        }

        file_descriptor fd_;
        filesystem::path path_name_;
    };

    auto mkstemp(std::string buffer_prefix) -> temporary_file_descriptor;
    auto mkstemp(std::string buffer_prefix, std::string buffer_postfix) -> temporary_file_descriptor;
}