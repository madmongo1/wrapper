//
// Created by Richard Hodges on 23/09/2018.
//

#pragma once
#include <wrapper/posix/system_error.hpp>

namespace wrapper::posix
{
    /// A base class providing services for handling errors indicated by errno
    ///
    struct handles_errno
    {
        [[noreturn]]
        static bool throw_errno(int number)
        {
            throw system::system_error(system::error_code(number, system::system_category()));
        }

        static int check_fd(int fd)
        {
            if (fd == -1) throw_errno(errno);
            return fd;
        }
    };
}