//
// Created by Richard Hodges on 25/09/2018.
//

#pragma once

#include <wrapper/core/optional.hpp>
namespace wrapper::posix
{

    using core::optional ;
}