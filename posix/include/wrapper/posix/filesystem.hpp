//
// Created by Richard Hodges on 24/09/2018.
//

#pragma once

#include <wrapper/core/filesystem.hpp>
#include <ios>

namespace wrapper::posix
{
    namespace filesystem = wrapper::core::filesystem;
}
