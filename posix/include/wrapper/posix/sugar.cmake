#sugar_files(INTERFACE_FILES_IN)

sugar_files(INTERFACE_FILES
        env.hpp
        file_descriptor.hpp file_descriptor_service.hpp
        file.hpp file_service.hpp
        filesystem.hpp
        handles_errno.hpp
        mmap.hpp
        optional.hpp
        system_error.hpp
        temporary_file_descriptor.hpp
        )

