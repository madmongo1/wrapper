//
// Created by Richard Hodges on 23/09/2018.
//

#pragma once

#include <wrapper/posix/file_descriptor.hpp>

namespace wrapper::posix
{

    struct memory_map_impl
    {
        using data_type = std::uint8_t;
        using pointer = data_type *;


        memory_map_impl(pointer p, std::size_t sz)
        : address_(p)
        , size_(sz)
        {}

        memory_map_impl()
        : memory_map_impl(nullptr, 0)
        {}

        void clear()
        {
            address_ = nullptr;
            size_ = 0;
        }

        pointer address() const { return address_; }
        std::size_t size() const { return size_; }
        bool valid() const { return address_ != nullptr; }

        void swap(memory_map_impl& r) noexcept
        {
            using std::swap;
            swap(address_, r.address_);
            swap(size_, r.size_);
        }
    private:

        pointer address_;
        std::size_t size_;
    };

    inline void swap(memory_map_impl& l, memory_map_impl& r) noexcept
    {
        l.swap(r);
    }

    struct memory_map_service
    {

    };

    struct memory_map
    {
        using implementation_type = memory_map_impl;
        using iterator = implementation_type::pointer;

        memory_map(file_descriptor_reference fd, std::streamoff offset = 0);

        memory_map(memory_map &&other) noexcept
        : memory_map(other.release())
        {
        }

        memory_map(memory_map const &other) = delete;

        memory_map &operator=(memory_map &&other) noexcept
        {
            auto tmp = std::move(other);
            swap(tmp);
            return *this;
        }

        memory_map &operator=(memory_map const &other) = delete;

        ~memory_map() noexcept;

        void unmap() noexcept;

        void swap(memory_map &other) noexcept
        {
            using std::swap;
            using ::wrapper::posix::swap;
            swap(impl_, other.impl_);
        }

        auto begin() -> iterator { return data(); }

        auto end() -> iterator { return begin() + size(); }

        auto data() -> iterator { return impl_.address(); }

        auto size() -> std::size_t { return impl_.size(); }

    private:

        using tuple_rep = std::tuple<std::uint8_t *, std::size_t>;

        memory_map(implementation_type impl) noexcept
        : impl_(std::move(impl))
        {

        }

        implementation_type release() noexcept
        {
            auto result = impl_;
            impl_.clear();
            return result;
        }

        implementation_type impl_;
    };

    inline
    memory_map mmap(file_descriptor_reference fd, std::streamoff offset = 0)
    {
        return memory_map(fd, offset);
    }
}

#include <boost/iostreams/categories.hpp>
#include <boost/iostreams/stream.hpp>

namespace wrapper::posix
{
    /// A Source Device for reading a memory mapped file when presented as chars
    struct mapped_byte_source
    {
        typedef char char_type;
        typedef boost::iostreams::source_tag category;

        mapped_byte_source(memory_map &ref)
        : mmap_(ref), current_(mmap_.begin()), end_(mmap_.end())
        {

        }

        std::streamsize read(char *s, std::streamsize n)
        {
            auto remaining = std::streamsize(end_ - current_);
            auto copied = std::min(n, remaining);
            if (copied > 0)
            {
                std::copy(current_, current_ + copied, s);
                current_ += copied;
            }
            else
            {
                copied = -1;
            }
            return copied;
        }

        memory_map &mmap_;
        memory_map::iterator current_, end_;
    };

    using mapped_byte_stream = boost::iostreams::stream<mapped_byte_source>;

    inline
    auto byte_stream(memory_map &mm) -> mapped_byte_stream
    {
        return mapped_byte_stream(mm);
    }

    inline
    auto byte_stream(memory_map &&mm) -> mapped_byte_stream
    {
        return byte_stream(mm);
    }
}