//
// Created by Richard Hodges on 24/09/2018.
//

#include "openmode.hpp"
#include <cassert>
#include <tuple>

namespace wrapper::posix
{
    std::string const &openmode_bit_to_string(int bitnum) {
        static thread_local auto buffer = std::string();
        switch (1 << bitnum) {
            case std::ios::in:
                buffer.assign("in");
                break;
            case std::ios::out:
                buffer.assign("out");
                break;
            case std::ios::ate:
                buffer.assign("ate");
                break;
            case std::ios::binary:
                buffer.assign("binary");
                break;
            case std::ios::app:
                buffer.assign("app");
                break;
            case std::ios::trunc:
                buffer.assign("trunc");
                break;
            default:
                buffer = "bit" + std::to_string(bitnum);
                break;
        }
        return buffer;
    }

    std::string openmode_to_string(std::ios::openmode supplied_mode) {
        constexpr auto digits = std::numeric_limits<std::ios::openmode>::digits;
        const char *const initial_sep = "";
        const char *sep = initial_sep;
        std::string result;
        auto mode = int(supplied_mode);
        for (int bit = 0; mode & (bit < digits); ++bit) {
            auto mask = 1 << bit;
            if (mode & mask) {
                result += sep;
                result += openmode_bit_to_string(bit);
                sep = " | ";
                mode &= mask;
            }
        }
        if (sep == initial_sep) {
            result.assign("null");
        }
        return result;
    }

    std::tuple<const char *, const char *> mode_tuple(std::ios::openmode mode) {
        switch (mode & (std::ios::in | std::ios::out | std::ios::app | std::ios::ate)) {
            //   pattern                                            if exists        if not exists
            case std::ios::in:                                   // read from start  fail
                return std::make_tuple("r", "rb");
            case std::ios::out | std::ios::trunc:                // truncate         create new
                return std::make_tuple("w", "wb");
            case std::ios::out | std::ios::app:                  // append to file   create new
                return std::make_tuple("a", "ab");
            case std::ios::in | std::ios::out:                   // read from start  error
                return std::make_tuple("r+", "r+b");
            case std::ios::out | std::ios::in | std::ios::trunc: // truncate         create new
                return std::make_tuple("w+", "w+b");
            case std::ios::out | std::ios::in | std::ios::app:   // write to end     create new
            case std::ios::in | std::ios::app:                   // write to end     create new
                return std::make_tuple("a+", "a+b");
            default:
                assert(!"invalid openmode bitmask");
                throw std::logic_error("invalid openmode bitmask: " + openmode_to_string(mode));
        }
    }

    const char *to_mode_string(std::ios::openmode mode) {
        auto text_or_bin = mode_tuple(mode);
        return mode & std::ios::binary
               ? std::get<1>(text_or_bin)
               : std::get<0>(text_or_bin);
    }
}