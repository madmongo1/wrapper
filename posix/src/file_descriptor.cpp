//
// Created by Richard Hodges on 07/09/2018.
//

#include <wrapper/posix/file_descriptor.hpp>
#include <unistd.h>
#include <wrapper/posix/mmap.hpp>

namespace wrapper::posix {
    auto dup(native_handle_base<file_descriptor_service> source) -> file_descriptor
    {

        return file_descriptor::adopt(file_descriptor::service_type::acquire_service().clone(source.handle()));
    }

    auto open(filesystem::path const& where, std::ios::openmode mode) -> file_descriptor
    {
        auto service = file_descriptor_service::acquire_service();
        auto handle = service.open(where, mode);
        return file_descriptor::adopt(handle);
    }

    auto gobble(file_descriptor_reference fd) -> std::string
    {
        auto here = tell(fd);
        auto map = mmap(fd, here);
        return std::string(map.begin(), map.end());
    }

    auto write(file_descriptor_reference fd, const void* buffer, std::size_t length) -> std::size_t
    try
    {
        auto written = ::write(fd.handle(), buffer, length);
        if (written == ssize_t(-1))
        {
            file_descriptor_service::throw_errno(errno);
        }
        return std::size_t(written);
    }
    catch(...)
    {
        std::throw_with_nested("write() failed");
    }

}