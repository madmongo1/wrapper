//
// Created by Richard Hodges on 01/09/2018.
//

#include <wrapper/posix/file_service.hpp>
#include <system_error>
#include <cassert>
#include <unistd.h>

namespace wrapper::posix {
    void file_service::throw_system_error(int eno)
    {
        throw std::system_error(std::error_code(eno, std::system_category()));
    }

    int file_service::validate_fd(int fd)
    {
        if (fd == -1)
            throw_system_error(errno);
        return fd;
    }

    auto file_service::validate_fp(std::FILE* fp) -> std::FILE*
    {
        if (fp == nullptr)
            throw_system_error(errno);
        return fp;
    }

    auto file_service::fileno(std::FILE *fp) -> int
    {
        return validate_fd(::fileno(fp));
    }

    auto file_service::dup(int fd) -> int
    {
        return validate_fd(::dup(fd));
    }

    auto file_service::fdopen(int fd, const char* mode) -> std::FILE *
    {
        return validate_fp(::fdopen(fd, mode));
    }

    void file_service::clone(implementation_type& target, implementation_type const& source)
    {
        assert(invalid(target));
        if (not invalid(source))
        {
            target.mode_ = source.mode_;
            target.fp_ = fdopen(dup(fileno(source.fp_)), target.mode_.c_str());
        }
    }

    void file_service::construct(implementation_type& impl, const char *path, std::string mode)
    {
        impl.mode_ = std::move(mode);
        impl.fp_   = validate_fp(std::fopen(path, impl.mode_.c_str()));
    }

    void file_service::destroy(implementation_type& impl)
    {
        if (invalid(impl))
        {

        }
        else
        {
            ::fclose(impl.fp_);
            impl = invalid();
        }
    }

}