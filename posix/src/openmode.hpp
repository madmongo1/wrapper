//
// Created by Richard Hodges on 24/09/2018.
//

#pragma once

#include <ios>

namespace wrapper::posix
{
    auto to_mode_string(std::ios::openmode mode) -> const char *;
}