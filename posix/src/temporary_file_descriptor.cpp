//
// Created by Richard Hodges on 28/09/2018.
//

#include <wrapper/posix/temporary_file_descriptor.hpp>

namespace wrapper::posix
{
    auto mkstemp(std::string buffer) -> temporary_file_descriptor
    {
        auto fds = file_descriptor_service::acquire_service();
        buffer += "XXXXXX";
        auto handle = fds.check_fd(::mkstemp(buffer.data()));
        return temporary_file_descriptor(file_descriptor::adopt(handle), filesystem::path(buffer));
    }
}