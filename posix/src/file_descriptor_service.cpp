//
// Created by Richard Hodges on 01/09/2018.
//

#include <wrapper/posix/file_descriptor_service.hpp>
#include <system_error>
#include <ciso646>
#include <cassert>
#include <unistd.h>
#include <boost/format.hpp>
#include <fcntl.h>

namespace wrapper::posix {
    auto file_descriptor_service::clone(native_handle_type source) -> native_handle_type {
        if (valid(source)) {
            auto target = ::dup(source);
            if (not valid(target)) {
                throw std::system_error(errno, std::system_category(), "file_descriptor_service::clone");
            }
            return target;
        } else {
            return invalid_handle_value();
        }
    }

    void file_descriptor_service::destroy(native_handle_type impl) noexcept {
        if (valid(impl)) {
            ::close(impl);
        }
    }

    struct open_flags {
        constexpr explicit open_flags(int value) : value_(value) {}

        constexpr operator int() const { return value_; }

    private:
        int value_;
    };

    struct open_mode {
        constexpr explicit open_mode(int value) : value_(value) {}

        constexpr operator int() const { return value_; }

    private:
        int value_;
    };

    namespace {


        auto get_open_mode(std::ios::openmode mode) -> open_mode {
            switch (mode & (std::ios::in | std::ios::out)) {
                case std::ios::in:
                    return open_mode(O_RDONLY);
                case std::ios::out:
                    return open_mode(O_WRONLY);
                case std::ios::in | std::ios::out:
                    return open_mode(O_RDWR);
                default:
                    throw std::logic_error("get_open_mode: invalid read/write combination");
            }
        }

        auto get_open_flags(std::ios::openmode mode) -> open_flags {
            constexpr auto trunc = std::ios::trunc;
            constexpr auto app = std::ios::app;

            auto result = 0;
            if (mode & (trunc | app))
                result |= O_CREAT;
            if (mode & trunc)
                result |= O_TRUNC;
            if (mode & app)
                result |= O_APPEND;
            return open_flags(result);
        }
    }

    auto file_descriptor_service::open(filesystem::path const &where, std::ios::openmode mode) -> native_handle_type
    try {
        auto handle = ::open(where.c_str(), get_open_flags(mode), get_open_mode(mode));
        if (invalid(handle))
            throw_errno(errno);
        return handle;
    }
    catch (...) {
        auto fmt = boost::format("file_descriptor_service::open(%1%, %2%) - failed") % where % mode;
        std::throw_with_nested(std::runtime_error(fmt.str()));
    }


    void file_descriptor_service::stat(native_handle_type handle, struct stat& target)
    {
        auto result = ::fstat(handle, std::addressof(target));
        if (result == -1) throw_errno(errno);
    }

}
