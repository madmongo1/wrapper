//
// Created by Richard Hodges on 24/09/2018.
//

#include <wrapper/posix/mmap.hpp>
#include <sys/mman.h>

namespace wrapper ::posix
{

    namespace
    {
        auto make_impl(file_descriptor_reference fd, std::streamoff offset) -> memory_map_impl
        {
            auto sz = stat(fd).size();
            if (offset > sz)
                throw std::invalid_argument("offset is larger than size of file");
            sz -= offset;
            auto addr = ::mmap(nullptr, sz, PROT_READ, MAP_SHARED, fd.handle(), ::off_t(offset));
            if (addr == (void *) -1)
                file_descriptor_service::throw_errno (errno);
            return memory_map_impl(static_cast<std::uint8_t *>(addr), std::size_t(sz));
        }
    }

    memory_map::memory_map(file_descriptor_reference fd, std::streamoff offset)
    try
    : impl_(make_impl(fd, offset))
    {
    }
    catch (...)
    {
        std::throw_with_nested(std::runtime_error("memory_map::memory_map() failed"));
    }

    memory_map::~memory_map() noexcept
    {
        unmap();
    }

    void memory_map::unmap() noexcept
    {
        if(impl_.valid())
        {
            ::munmap(impl_.address(), impl_.size());
            impl_.clear();
        }
    }


}