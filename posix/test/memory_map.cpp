//
// Created by Richard Hodges on 27/09/2018.
//

#include <gtest/gtest.h>

#include <wrapper/posix/mmap.hpp>
#include <wrapper/posix/temporary_file_descriptor.hpp>
#include <string_view>

using namespace wrapper;
using namespace std::literals;

TEST(memory_map, read_file)
{
    auto tmp = posix::mkstemp("baloney");

    auto test_data = "muppet\n"sv;
    posix::write(tmp.fd(), test_data.data(), test_data.size());
    auto mapping = mmap(tmp.fd());
    auto mapped_data = std::string_view(reinterpret_cast<const char*>(mapping.data()), mapping.size());
    ASSERT_EQ(test_data, mapped_data);
}
