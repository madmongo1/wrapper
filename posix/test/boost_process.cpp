//
// Created by Richard Hodges on 05/09/2018.
//

#include <gtest/gtest.h>

#include <boost/process.hpp>

namespace proc = boost::process;
namespace fs = boost::filesystem;

TEST(boost_process, system)
{

    auto bash = fs::path("/bin/bash");
    int result = proc::system(bash, "-c", "echo hello");
    EXPECT_EQ(0, result);

    auto env = boost::this_process::environment();
    for (auto&& entry : env)
    {
        std::cout << entry.get_name() << " = " << entry.to_string() << '\n';
    }
}
