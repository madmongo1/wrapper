//
// Created by Richard Hodges on 06/09/2018.
//

#include "configuration.hpp"

namespace app
{

    nlohmann::json to_json(configuration const &config)
    {
        auto result = nlohmann::json{{"version", config.version},
                                     {"name",    config.name}};
        return result;
    }

    void from_json(nlohmann::json const &l, configuration &config)
    {
        config.complete = false;
        config.name = l.at("name").get<std::string>();
        config.version = l.at("version").get<int>();
        config.complete = true;
    }

    std::ostream &operator<<(std::ostream &os, configuration const &conf)
    {
        return os << to_json(conf).dump(4, ' ', true);
    }

    std::istream &operator>>(std::istream &is, configuration &conf)
    {
        from_json(nlohmann::json::parse(is), conf);
        return is;
    }

}
