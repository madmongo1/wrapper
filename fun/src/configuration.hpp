//
// Created by Richard Hodges on 06/09/2018.
//

#pragma once

#include <nlohmann/json.hpp>
#include <string>

namespace app
{

    struct configuration
    {
        int version;
        std::string name;
        bool complete = false;

        configuration &make_default()
        {
            version = 1;
            name = "un-named";
            return *this;
        }

    };

    nlohmann::json to_json(configuration const &config);

    void from_json(nlohmann::json const &l, configuration &config);


    std::ostream &operator<<(std::ostream &os, configuration const &conf);

    std::istream &operator>>(std::istream &is, configuration &conf);

    template<class T, std::enable_if<std::is_same_v<configuration, std::decay_t<T>>> * = nullptr>
    auto operator<<(T &&conf, std::istream &is) -> decltype(auto)
    {
        is >> conf;
        return std::forward<T>(conf);
    }

    template<class T, std::enable_if<std::is_same_v<configuration, std::decay_t<T>>> * = nullptr>
    auto operator>>(T &&conf, std::ostream &os) -> decltype(auto)
    {
        os << conf;
        return std::forward<T>(conf);
    }

}



