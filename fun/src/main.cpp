//
// Created by Richard Hodges on 06/09/2018.
//
#include "configuration.hpp"
#include "fs/fs.hpp"
#include <nlohmann/json.hpp>
#include <iostream>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/format.hpp>

namespace app
{
    auto operator"" _fmt(char const* p, std::size_t)
    {
        return boost::format(p);
    }

    auto log()
    {
        auto stream = append(fs::create_directories_in_path(fs::pwd() / "config" / "log.txt"));
        auto now = boost::posix_time::microsec_clock::local_time();
        stream << to_iso_string(now) << " : ";
        return stream;
    }

    auto load_config(fs::path const &path = fs::pwd() / "config" / "config.json")
    {
        auto result = configuration();

        if (exists(path))
        {
            log() << "loading config from %1%\n"_fmt % path;
            read(path) >> result;
        }
        else
        {
            log() << "writing new config to %1%\n"_fmt % path;
            replace(create_directories_in_path(path)) << result.make_default();
        }

        return result;
    }
}

int main()
{
    try
    {
        using namespace app;
        auto config = load_config();
        std::cout << "config = " << config << std::endl;
    }
    catch(std::system_error& se)
    {
        std::cerr << "exception: " << se.code().value() << " : " << se.code().message() << std::endl;
    }
    catch(std::exception& e)
    {
        std::cerr << "exception: " << e.what() << std::endl;
    }

}