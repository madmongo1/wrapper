//
// Created by Richard Hodges on 06/09/2018.
//

#pragma once

#include <boost/filesystem.hpp>
#include <utility>
#include <ciso646>

namespace app::fs
{

    namespace underlying_fs = ::boost::filesystem;
    using namespace underlying_fs;

    template<class Path, std::enable_if_t<std::is_same_v<path, std::decay_t<Path>>> * = nullptr>
    auto create_directories_in_path(Path &&p) -> decltype(auto)
    {
        underlying_fs::create_directories(p.parent_path());
        return std::forward<Path>(p);
    }

    inline auto pwd() -> path
    {
        return underlying_fs::current_path();
    }

    namespace detail
    {
        template<class Stream>
        auto set_stream_exceptions(Stream &&stream, bool set) -> decltype(auto)
        {
            if (set)
            {
                stream.exceptions(std::ios::badbit | std::ios::failbit);
            }
            return std::forward<Stream>(stream);
        }
    }

    inline auto replace(path const &p, bool with_exceptions = true) -> std::ofstream
    {
        return detail::set_stream_exceptions(std::ofstream(p.string(), std::ios::out | std::ios::trunc),
                                             with_exceptions);
    }

    inline auto append(path const &p, bool with_exceptions = true) -> std::ofstream
    {
        auto &&str = p.string();
        auto fs = detail::set_stream_exceptions(std::ofstream(str, std::ios::out | std::ios::app), with_exceptions);
        return fs;
    }

    inline auto read(path const &p, bool with_exceptions = true) -> std::ifstream
    {
        return detail::set_stream_exceptions(std::ifstream(p.string(), std::ios::in), with_exceptions);
    }
}

using app::fs::append;
using app::fs::create_directories_in_path;
using app::fs::read;
using app::fs::replace;

