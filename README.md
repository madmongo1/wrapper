Wrapper is a library designed to wrap c libraries in a type-safe, 
exception-safe, raai interface so you can write efficient c++ in the same
style you'd use to write python or javascript.

It's a work in progress and contributors are welcome.

example:

```
        auto config_json = json::parse(byte_stream(mmap(posix::open(deduce_config_file(vm)))));
```
