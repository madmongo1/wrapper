//
// Created by Richard Hodges on 26/07/2018.
//

#include <gtest/gtest.h>
#include <wrapper/core/adopted_pointer.hpp>
#include <memory>
#include <cstdlib>

namespace {
    struct mock_memory
    {
        std::aligned_storage<1024> store_;
        int                        allocated = 0, freed = 0;

        void *malloc(std::size_t)
        {
            ++allocated;
            return std::addressof(store_);
        }

        void free(void *p)
        {
            ++freed;
            if (p != std::addressof(store_))
            {
                throw std::invalid_argument("wrong pointer freed");
            }
        }

        bool matching_counts() const
        {
            return allocated == freed;
        }

        ::testing::AssertionResult check_matching_counts() const
        {
            auto result = ::testing::AssertionSuccess();

            if (!matching_counts())
            {
                result = ::testing::AssertionFailure();
                result << "allocated=" << allocated << ", freed=" << freed;
            }

            return result;
        }

        ::testing::AssertionResult check_address(const void *pv) const
        {
            auto result = ::testing::AssertionSuccess();

            auto p     = reinterpret_cast<const std::uint8_t *>(pv);
            auto pdata = reinterpret_cast<const std::uint8_t *>(std::addressof(store_));

            if (p != pdata)
            {
                result = ::testing::AssertionFailure();
                result << "p=" << p << ", pdata=" << pdata;
            }

            return result;
        }


        auto get_free()
        {
            return [this](auto *p)
            {
                this->free(p);
            };
        }

    };

}


TEST(adopted_pointer, basics)
{
    auto mem = mock_memory();
    auto vp  = mem.malloc(sizeof(int));
    auto ap  = wrapper::core::adopt(vp);

    ASSERT_FALSE(ap.empty());

    auto up = to_unique(std::move(ap), mem.get_free());
    ASSERT_TRUE(up);
    ASSERT_TRUE(ap.empty());
    up.reset();
    ASSERT_TRUE(mem.check_matching_counts());
}

TEST(adopted_pointer, casts_properly)
{
    auto mem = mock_memory();
    auto vp  = mem.malloc(sizeof(int));
    auto avp = wrapper::core::adopt(vp);
    ASSERT_FALSE(avp.empty());

    auto aip = std::move(avp).cast<int>();
    ASSERT_TRUE(avp.empty());
    ASSERT_FALSE(aip.empty());
    auto up = to_unique(std::move(aip), mem.get_free());
    ASSERT_TRUE(up);
    ASSERT_TRUE(aip.empty());
    up.reset();
    ASSERT_TRUE(mem.check_matching_counts());
}

TEST(adopted_pointer, chaining)
{
    auto mem = mock_memory();
    auto up  = to_unique(wrapper::core::adopt(mem.malloc(sizeof(int))).cast<int>(),
                         mem.get_free());
    ASSERT_TRUE(mem.check_address(up.get()));
    up.reset();
    ASSERT_TRUE(mem.check_matching_counts());
}
