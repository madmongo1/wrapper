//
// Created by Richard Hodges on 23/07/2018.
//



#include <gtest/gtest.h>
#include <wrapper/core/concept/Service.hpp>

TEST(service_concept, has_implementation_type)
{
    struct test_service_1
    {
        using implementation_type = char*;
    };

    EXPECT_TRUE(wrapper::core::concept::has_implementation_type<test_service_1>());

    struct test_service_2
    {

    };

    EXPECT_FALSE(wrapper::core::concept::has_implementation_type<test_service_2>());
}