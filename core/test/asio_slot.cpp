//
// Created by Richard Hodges on 27/07/2018.
//

#include <gtest/gtest.h>
#include <boost/asio.hpp>
#include <boost/signals2.hpp>
#include <memory>
#include <wrapper/notstd/apply.hpp>

/*

namespace asio {

    using namespace boost::asio;
    namespace system = boost::system;

    struct subscription_impl
    {

    };

    struct subscription
    {
        using ptr_type = std::shared_ptr<subscription_impl>;

        ptr_type ptr_;
    };


    template<class Sig>
    struct signal_impl
    {
        using weak_ptr_type = std::weak_ptr<subscription_impl>;

        std::mutex mutex_;
        std::set<weak_ptr_type, std::owner_less<weak_ptr_type>> cache_;



    };

    template<class Sig>
    struct signal_service
        : asio::detail::service_base<signal_service<Sig>>
    {
        using implementation_class = signal_impl<Sig>;
        using implementation_type = std::shared_ptr<implementation_class>;

        signal_service(io_context& owner)
            : asio::detail::service_base<signal_service<Sig>>::service_base(owner) {}

        void construct(implementation_type& impl)
        {
            impl = std::make_shared<implementation_class>();
        }

        void destroy(implementation_type& impl)
        {
            impl.reset();
        }

        // services

        template<class SubscriptionHandler>
        subscription subscribe(implementation_type& impl, SubscriptionHandler&& handler)
        {
            return subscription();
        }

        template<class...Margs>
        void invoke(implementation_type& impl, Margs&&...args)
        {

        }

    private:
        void shutdown_service() override
        {

        }

    };

    template<class Sig>
    struct signal;

    template<class Ret, class...Args>
    struct signal<Ret(Args...)>
        : asio::basic_io_object<signal_service<Ret(Args...)>>
    {

        using function_signature = Ret(Args...);
        using base_class = asio::basic_io_object<signal_service<function_signature>>;

        signal(io_context& owner)
            : base_class(owner)
        {

        }

        template<class SubscriptionHandler>
        subscription subscribe(SubscriptionHandler&& handler)
        {
            return this->get_service()
                       .subscribe(this->get_implementation(), std::forward<SubscriptionHandler>(handler));
        }

        template<class...Margs>
        void invoke(Margs&& ...args)
        {
            this->get_service().invoke(this->get_implementation(), std::forward<Margs>(args)...);
        }

    };

    template<class Sig>
    struct slot_impl
    {

    };

    template<class Sig>
    struct slot_service
        : asio::detail::service_base<signal_service<Sig>>
    {
        using implementation_type = slot_impl<Sig>;

        slot_service(io_context& owner)
            : asio::detail::service_base<signal_service<Sig>>::service_base(owner) {}

        void construct(implementation_type& impl)
        {
        }

        void destroy(implementation_type& impl)
        {
        }

    private:
        void shutdown_service() override
        {

        }
    };

    template<class Sig>
    struct slot;

    template<class Ret, class...Args>
    struct slot<Ret(Args...)>
        : asio::basic_io_object<slot_service<Ret(Args...)>>
    {

        using function_signature = Ret(Args...);
        using base_class = asio::basic_io_object<signal_service<function_signature>>;

        slot(io_context& owner)
            : base_class(owner)
        {

        }
    };


}

template<class Sig>
struct handler_result;

template<class Ret, class...Args>
struct handler_result<Ret(Args...)>
{
    using arg_tuple = std::tuple<std::decay_t<Args>...>;
    using opt_arg_tuple = boost::optional<arg_tuple>;

    using bool_type =::testing::AssertionResult;


    handler_result()
        : results_()
    {
    }

    template<class...CArgs>
    handler_result(std::tuple<CArgs...>&& args)
        : results_(std::move(args))
    {

    }

    template<class...CArgs>
    bool_type set(std::tuple<CArgs...>&& args)
    {
        if (results_.is_initialized())
        {
            return ::testing::AssertionFailure(::testing::Message("result already set"));
        }
        else
        {
            auto it = [this](auto&& ...args)
            {
                results_.emplace(std::forward<decltype(args)>(args)...);
            };
            wrapper::notstd::apply(it, std::move(args));
        }
    }


private:
    opt_arg_tuple results_;
};

template<class Sig>
struct handler_results;

template<class R, class...Args>
struct handler_results<R(Args...)>
{


};

TEST(asio_slot, basics)
{
    auto executor = asio::io_context();
    auto sig      = asio::signal<void()>(executor);

    auto subscription = sig.subscribe([&](asio::system::error_code const&)
                                      {

                                      });
    sig.invoke();

    try
    {
        auto count = executor.poll_one();
        EXPECT_EQ(0, count);
    }
    catch (std::exception& e)
    {
        FAIL() << e.what();
    }
}


*/
