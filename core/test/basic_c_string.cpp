//
// Created by Richard Hodges on 19/07/2018.
//

#include <gtest/gtest.h>
#include <wrapper/c_string.hpp>

TEST(basic_c_string, basics)
{
    const char source[] = "the quick brown fox jumps over the lazy dog";

    // prove we took a copy
    auto p = wrapper::c_string(wrapper::adopt(strdup(source)));
    ASSERT_NE(p.c_str(), static_cast<const char*>(source));

    std::ostringstream oss;
    oss << p;
    ASSERT_EQ("the quick brown fox jumps over the lazy dog", oss.str());

    std::ostringstream().swap(oss);
    oss << std::right << std::setw(50) << p;
    ASSERT_EQ("       the quick brown fox jumps over the lazy dog", oss.str());

    auto p2 = std::move(p);
    ASSERT_NE(p, p2);

    ASSERT_TRUE(p.empty());
    ASSERT_TRUE(p.invalid());
    ASSERT_EQ(0, p.size());

    ASSERT_FALSE(p2.empty());
    ASSERT_FALSE(p2.invalid());
    ASSERT_EQ(43, p2.size());
}