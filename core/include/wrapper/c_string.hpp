//
// Created by Richard Hodges on 19/07/2018.
//

#pragma once

#include <wrapper/core/basic_c_string.hpp>
#include <wrapper/core/malloc_c_string_service.hpp>
#include <wrapper/adopt.hpp>

namespace wrapper
{
    using c_string = core::basic_c_string<core::malloc_c_string_service<char>>;
    using const_c_string = core::basic_c_string<core::malloc_c_string_service<const char>>;
}