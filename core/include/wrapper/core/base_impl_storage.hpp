//
// Created by Richard Hodges on 21/07/2018.
//

#pragma once

#include <wrapper/core/service_storage_by_constexpr.hpp>
#include <wrapper/core/service_storage_by_reference.hpp>
#include <wrapper/core/invoke_constructor.hpp>

namespace wrapper { namespace core {


    template<class Service>
    using service_storage = std::conditional_t <
    std::is_empty<Service>::value,
    service_storage_by_constexpr<Service>,
    service_storage_by_reference<Service>
    >;


    /// Defines the base storage mechanism for a service's implementation_type.
    /// @see wrapper::core::Service
    /// This class provides the ability to locate a service for a given handle and use that service to construct,
    /// destroy and move an implementation.
    template<class Service>
    struct base_impl_storage
        : service_storage<Service>
    {
        using service_storage_type = service_storage<Service>;
        using service_type = typename service_storage_type::service_type;
        using implementation_type = typename service_type::implementation_type;

        static_assert(concept::is_service<Service>(), "Service concept not satisfied");

        template<class...Args>
        constexpr static auto service_args(Args&&...args)
        {
            return std::forward_as_tuple(std::forward<Args>(args)...);
        }

        template<class...Args>
        constexpr static auto impl_args(Args&&...args)
        {
            return std::forward_as_tuple(std::forward<Args>(args)...);
        }


        template<class...ServiceArgs, class...ConstructArgs>
        base_impl_storage(std::tuple<ServiceArgs...>&& service_args, std::tuple<ConstructArgs...> construct_args)
            : service_storage_type(invoke_acquire_service(std::move(service_args)))
            , impl_(service().invalid())
        {
            invoke_construct(std::move(construct_args));
        };

        base_impl_storage(base_impl_storage const& other) = delete;

        base_impl_storage(base_impl_storage&& other) noexcept
            : service_storage_type(std::move(other))
            , impl_(service().invalid())
        {
            service().move(impl(), other.impl());
        }

        base_impl_storage& operator =(base_impl_storage const& other) = delete;

        base_impl_storage& operator =(base_impl_storage&& other) noexcept
        {
            auto tmp = base_impl_storage(std::move(other));
            swap(tmp);
            return *this;
        }

        ~base_impl_storage()
        {
            auto&& svc = service();
            if (!svc.invalid(impl()))
                service().destroy(impl());
        }

        auto swap(base_impl_storage& other) noexcept
        {
            service_storage_type::swap(other);
            std::swap(impl(), other.impl());
        }

        using service_storage_type::service;

        implementation_type& impl() { return impl_; }

        implementation_type const& impl() const { return impl_; }

    protected:
        /// A flag type to indicate that the constructor should copy the service
        /// from another instance
        struct copy_service
        {
        };

        /// Copy the service from another instance but default-construct the implementation
        base_impl_storage(copy_service, base_impl_storage const& other)
            : service_storage_type(other)
            , impl_(service().invalid())
        {

        }

    private:
        template<class...ConstructArgs>
        void invoke_construct(std::tuple<ConstructArgs...>&& args)
        {
            constexpr auto argN = sizeof...(ConstructArgs);
            invoke_construct(std::make_index_sequence<argN>(), std::move(args));
        }

        template<std::size_t...Is, class Tuple>
        void invoke_construct(std::index_sequence<Is...>, Tuple&& tup)
        {
            service().construct(impl(), std::get<Is>(std::forward<Tuple>(tup))...);
        }
        template<class...ConstructArgs>

        decltype(auto) invoke_acquire_service(std::tuple<ConstructArgs...>&& args)
        {
            constexpr auto argN = sizeof...(ConstructArgs);
            return invoke_acquire_service(std::make_index_sequence<argN>(), std::move(args));
        }

        template<std::size_t...Is, class Tuple>
        decltype(auto) invoke_acquire_service(std::index_sequence<Is...>, Tuple&& tup)
        {
            return service_type::acquire_service(std::get<Is>(std::forward<Tuple>(tup))...);
        }

    private:
        implementation_type impl_;
    };
}}
