//
// Created by Richard Hodges on 25/09/2018.
//

#pragma once
#include <ciso646>
#include <cassert>
#include <cstdio>
#include <cstring>
#include <ostream>
#include <type_traits>

namespace wrapper::core
{
    /// Models a c-style string which may be unset. The string'd data is not owned by this object
    /// and must outlive all uses of the object
    struct cstring_view
    {
        using char_type = const char;
        using pointer = std::add_pointer_t<char_type>;
        using reference = std::add_lvalue_reference_t<char_type>;

        /// A bidrectional iterator for scanning the view of the string
        using iterator = std::add_pointer_t<char_type>;
        using const_iterator = std::add_pointer_t<std::add_const_t<char_type>>;

        /// Create an invalid cstring_view
        constexpr explicit cstring_view()
        : ptr_(nullptr)
        {}

        /// Create a cstring_view initialised with a valid pointer to c-style string.
        /// @param ptr is a pointer to a non-null c string
        /// @post this->valid() == (ptr != nullptr)
        /// @note the c-string must remain valid for the lifetime of this object
        ///
        constexpr explicit cstring_view(pointer ptr)
        : ptr_(ptr)
        {
        }

        /// Assign a new c string pointer to the cstring_view
        /// @param newptr is a pointer to the new c-style string. It may be nullptr.
        /// @post this->valid() == (ptr != nullptr)
        /// @note the c-string must remain valid for the lifetime of this object
        ///
        constexpr cstring_view& operator=(pointer newptr) noexcept
        {
            ptr_ = newptr;
            return *this;
        }

        /// Indicate whether this object is proxying a valid c style string
        /// @returns true if the internal pointer != nullptr
        ///
        constexpr bool valid() const {
            return bool(ptr_);
        }

        /// Indicate that the internal c-string pointer is null.
        /// @returns true if the internal pointer == nullptr
        ///
        constexpr bool invalid() const {
            return not valid();
        }

        /// Return a pointer to the first character of the c-string
        /// @pre this->valid() == true
        /// @returns address of the first character of the string
        ///
        constexpr auto c_str() const -> pointer
        {
            return data();
        }

        /// Return a pointer to the first character of the c-string
        /// @pre this->valid() == true
        /// @returns address of the first character of the string
        ///
        constexpr auto data() const -> pointer
        {
            assert(valid());
            return ptr_;
        }

        /// Return the size of the string in characters
        /// @returns valid() ? 0 : std::strlen(data())
        ///
        auto size() const -> std::size_t
        {
            return valid() ? std::strlen(data()) : 0;
        }

        /// Return an iterator referencing the first character of the c-string
        /// @pre valid()
        /// @returns an iterator pointing to the first character
        ///
        auto begin() const -> iterator
        {
            return data();
        }

        /// Return an iterator referencing the trailing zero of the c-string
        /// @pre valid()
        /// @returns an iterator pointing to the trailing zero
        ///
        auto end() const -> iterator
        {
            return data() + size();
        }

        /// cstring_views are logically true when valid()
        /// @returns true if valid() == true, false if valid() == false
        ///
        constexpr operator bool() const
        {
            return valid();
        }

        /// cstring_views are logically true when valid()
        /// @returns false if valid() == true, true if valid() == false
        ///
        constexpr bool operator not() const
        {
            return not valid();
        }

        /// Allow implicit conversion to a c-string pointer for use in api calls
        operator pointer() const
        {
            return data();
        }

    private:
        pointer ptr_;
    };

    inline
    auto compare(cstring_view l, cstring_view r) -> int
    {
        if (l.valid() && r.valid())
            return std::strcmp(l.data(), r.data());
        if (l.valid() and not r.valid())
            return 1;
        if (not l.valid() and r.valid())
            return -1;
        return ::strcmp(l, r);
    }

    inline
    auto operator==(cstring_view l, cstring_view r) -> bool
    {
        return compare(l, r) == 0;
    }

    inline
    auto operator==(cstring_view l, const char* r) -> bool
    {
        return l == cstring_view(r);
    }
    inline
    auto operator==(const char* l, cstring_view r) -> bool
    {
        return cstring_view(l) == r;
    }




    /// Given a cstring_view, return a c-style string that is guaranteed to be a valid string suitable for debugging
    /// @param view is a cstring_view
    /// @returns const char* which is a guaranteed not to be a nullptr
    ///
    inline
    auto safe_representation(cstring_view view) -> const char*
    {
        return view.valid() ? view.data() : "{null}";
    }

    /// Given a cstring_view, return a c-style string that is guaranteed to be a valid string.
    /// If view is valid, returns view.c_str(). Otherwise returns the address of ""
    /// @param view is a cstring_view
    /// @returns non-null const char*
    ///
    inline
    auto safe(cstring_view view) -> const char*
    {
        return view.valid() ? view.data() : "";
    }

    /// Stream a cstring_view to a std::ostream
    /// @param os is a reference to the ostream
    /// @param view is a cstring_view which may or may not be valid
    /// @returns a reference to os
    ///
    inline
    std::ostream& operator<<(std::ostream& os, cstring_view view)
    {
        return os << safe_representation(view);
    }

    inline
    auto to_string(cstring_view l) -> std::string
    {
        auto result = std::string(safe(l));
        return result;
    }

    inline
    auto operator+(cstring_view l, cstring_view r) -> std::string
    {
        return to_string(l) + safe(r);
    }

    inline
    auto operator+(const char* l, cstring_view r) -> std::string
    {
        return cstring_view(l) + r;
    }

    inline
    auto operator+(cstring_view l, const char* r) -> std::string
    {
        return l + cstring_view(r);
    }

}