//
// Created by Richard Hodges on 19/07/2018.
//

#pragma once

namespace wrapper { namespace core {

    /// Provide common operations for a service which is implemented in terms of c-style string pointers.
    /// This class is designed to be a base class of a Service
    /// @see wrapper::core::Service
    ///
    template<class Char>
    struct c_string_ops
    {
        using char_type = Char;
        using const_char_type = std::add_const_t<char_type>;
        using implementation_type = char_type *;

        /// Move the src implementation to the dest implementation
        /// @pre invalid(dest) == true
        /// @post invalid(src) == true
        /// @exception noexcept
        auto move(implementation_type& dest, implementation_type& src) noexcept -> void
        {
            assert(invalid(dest));
            dest = src;
            src  = invalid();
        }

        constexpr implementation_type invalid() noexcept
        {
            return nullptr;
        }

        constexpr bool invalid(implementation_type const& impl) noexcept
        {
            return impl == invalid();
        }

        const_char_type *data(implementation_type const& impl) noexcept
        {
            return impl;
        }

        char_type *data(implementation_type& impl) noexcept
        {
            return impl;
        }

        int compare(implementation_type const& l, implementation_type const& r) noexcept
        {
            if (l == r) return 0;
            if (invalid(l) and !invalid(r)) return -1;
            if (!invalid(l) and invalid(r)) return 1;
            return strcmp(l, r);
        }

        int compare(implementation_type const& l, const_char_type *r) noexcept
        {
            if (l == r) return 0;
            if (invalid(l) and bool(r)) return -1;
            if (!invalid(l) and !bool(r)) return 1;
            return strcmp(l, r);
        }

        bool is_less(implementation_type const& l, implementation_type const& r) noexcept
        {
            return compare(l, r) < 0;
        }

        bool is_less_equal(implementation_type const& l, implementation_type const& r) noexcept
        {
            return compare(l, r) <= 0;
        }

        bool is_equal(implementation_type const& l, implementation_type const& r) noexcept
        {
            return compare(l, r) == 0;
        }

        bool not_equal(implementation_type const& l, implementation_type const& r) noexcept
        {
            return compare(l, r) != 0;
        }

        bool is_greater_equal(implementation_type const& l, implementation_type const& r) noexcept
        {
            return compare(l, r) >= 0;
        }

        bool is_greater(implementation_type const& l, implementation_type const& r) noexcept
        {
            return compare(l, r) > 0;
        }

        std::size_t size(implementation_type const& impl) noexcept
        {
            return invalid(impl) ? 0 : strlen(impl);
        }
    };

    //
    template<class NextObject>
    struct basic_c_string_object_interface
        : NextObject
    {
        using NextObject::NextObject;

        using service_type = typename NextObject::service_type;
        using char_type = typename service_type::char_type;
        using const_char_type = typename service_type::const_char_type;

        static_assert(std::is_base_of<c_string_ops<char_type>, service_type>(), "");

        using NextObject::service;
        using NextObject::impl;

        int compare(const_char_type *r) const
        {
            return service().compare(impl(), r);
        }

        bool operator ==(basic_c_string_object_interface const& other) const
        {
            return service().compare(impl(), other.impl()) == 0;
        }
    };

    template<class NextObject>
    bool operator ==(basic_c_string_object_interface<NextObject> const& l,
                     typename basic_c_string_object_interface<NextObject>::const_char_type *r)
    {
        return l.compare(r) == 0;
    }

    template<class NextObject>
    bool operator ==(typename basic_c_string_object_interface<NextObject>::const_char_type *l,
                     basic_c_string_object_interface<NextObject> const& r)
    {
        return (r.compare(l) * -1) == 0;
    }


}}