//
// Created by Richard Hodges on 21/07/2018.
//

#pragma once

namespace wrapper { namespace core {

    template<class Service>
    struct service_storage_by_reference
    {
        using service_type = Service;

        service_storage_by_reference(service_type& service)
            : service_(std::addressof(service))
        {}

        auto service() -> service_type&
        {
            return *service_;
        }

        void swap(service_storage_by_reference& other) noexcept
        {
            std::swap(service_, other.service_);
        }

        service_type *service_;
    };

}}
