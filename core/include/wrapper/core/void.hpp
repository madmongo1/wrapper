//
// Created by Richard Hodges on 23/07/2018.
//

#pragma once

#include <wrapper/core/config.hpp>

namespace wrapper { namespace core {

    namespace detail {

        /// A nullary metafunction which always yields the type void
        template<class T>
        struct voider
        {
            using type = void;
        };
    }

    /// Invoke the nullary metafunction detail::voider
    template<class T> using void_t = typename detail::voider<T>::type;

}}