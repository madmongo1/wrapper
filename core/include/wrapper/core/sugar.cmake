sugar_include(concept)

sugar_files(INTERFACE_FILES_IN
        config.hpp.in
        filesystem.hpp.in
        git_info.hpp.in
        optional.hpp.in
        system_error.hpp.in
        variant.hpp.in)

sugar_files(INTERFACE_FILES
        adopted_pointer.hpp

        base_impl_storage.hpp

        basic_c_string.hpp
        c_string_ops.hpp
        cstring_view.hpp

        choose_impl_storage.hpp
        cloning_impl_storage.hpp

        documentation_defs.hpp

        invoke_constructor.hpp

        malloc_c_string_service.hpp

        print.hpp

        service_storage_by_constexpr.hpp
        service_storage_by_reference.hpp

        void.hpp)

