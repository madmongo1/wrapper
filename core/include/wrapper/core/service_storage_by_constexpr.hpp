//
// Created by Richard Hodges on 21/07/2018.
//

#pragma once
#include "wrapper/core/concept/Service.hpp"

namespace wrapper { namespace core {

    template<class Service>
    struct service_storage_by_constexpr
    {
        using service_type = Service;

        static_assert(::wrapper::core::concept::is_service<service_type>(), "");

        constexpr service_storage_by_constexpr(service_type) noexcept {}

//        constexpr service_storage_by_constexpr(std::tuple<>&&) noexcept {}

        void swap(service_storage_by_constexpr& other) noexcept
        {
            // nop
        }

        static constexpr auto service() -> service_type
        {
            return service_type();
        }
    };
}}