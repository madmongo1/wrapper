/// @file adopted_pointer.hpp
/// Defines the template class adopted_pointer and associated free functions.
/// @authors Richard Hodges hodges.r@gmail.com
/// @copyright (C) 2018 Richard Hodges hodges.r@gmail.com
//
// Created by Richard Hodges on 19/07/2018.
//

#pragma once

#include <cassert>
#include <type_traits>
#include <memory>

namespace wrapper { namespace core {

    /// Wraps a raw pointer to indicate that the receiver should take ownership.
    /// This class is intended to wrap pointers returned by external libraries.
    /// An adopted pointer will assert if it is destroyed while it still owns the pointer.
    /// Adopted pointers may be move-constructed or move-assigned from each other, but they may not be copied or assigned.
    /// @ingroup Core
    /// @tparam T is the type of the pointer's target.
    template<class T>
    struct adopted_pointer
    {
        /// Resolves to the type of the pointer's target
        using element_type = T;

        /// Resolves to the type of the raw pointer
        using pointer = std::add_pointer_t<element_type>;

        /// Construct an adopted pointer
        /// @param p is the source pointer delivered by the external library.
        /// @note nullptr is a permissible value for the pointer which indicates that the adopted_pointer is empty.
        ///       The only logical action after this is to move-assign from another adopted_pointer
        ///
        explicit constexpr adopted_pointer(T *p = nullptr) noexcept
            : ptr_(p)
        {
        }

        /// Copy constructor is deleted
        adopted_pointer(adopted_pointer const& other) = delete;

        /// assignment operator is deleted
        adopted_pointer& operator =(adopted_pointer const&& other) = delete;

        /// Move-construction is permitted
        constexpr adopted_pointer(adopted_pointer&& other) noexcept
            : ptr_(other.ptr_)
        {
            other.ptr_ = nullptr;
        }

        /// Assignment is permitted if this object is empty
        /// @param other is an rvalue reference to the donor adopted_ptr
        /// @pre this->empty() == true
        /// @post this->empty() == false
        /// @post other.empty() == true
        ///
        constexpr adopted_pointer& operator =(adopted_pointer&& other) noexcept
        {
            assert(empty());
            ptr_ = other.ptr_;
            other.ptr_ = nullptr;
            return *this;
        }

        /// destroy the adopted_ptr.
        /// @pre this->empty() == true
        ~adopted_pointer() noexcept
        {
            assert(empty());
        }

        /// Indicate whether the adopted_ptr is empty.
        /// Empty is defined as not representing an external pointer.
        constexpr bool empty() const
        {
            return ptr_ == nullptr;
        }

        /// Allow a caller to claim ownership of the object pointed to.
        /// @returns a pointer
        /// @pre this->empty() == false
        /// @post this->empty()
        ///
        [[nodiscard]]
        constexpr pointer get()&& noexcept
        {
            assert(!empty());
            auto result = ptr_;
            ptr_ = nullptr;
            return result;
        }

        /// Cast this adopted_pointer to an adopted_pointer of another type, as if by reinterpret_cast<>.
        /// This method is designed to cast the return value of a function which returns a void*, such as malloc().
        /// @tparam To is the type of object that the resulting adopted_pointer should own.
        /// @returns adopted_pointer<To>
        /// @post this->empty() == true
        ///
        template<class To>
        [[nodiscard]]
        constexpr auto cast() &&
        {
            using result_type = adopted_pointer<To>;
            using result_pointer = typename result_type::pointer;
            auto result = adopted_pointer<To>(static_cast<result_pointer>(ptr_));
            ptr_ = nullptr;
            return result;
        }

    private:
        pointer ptr_;
    };

    /// Construct an adopted_pointer of the correct type from a native raw pointer.
    /// @ingroup CoreFunctions
    /// @tparam T is the type of object pointed to
    /// @param p is the raw pointer
    /// @returns adopted_pointer<T>
    /// @relates adopted_pointer
    ///
    template<class T>
    auto adopt(T *p) -> adopted_pointer<T>
    {
        assert(p);
        return adopted_pointer<T>(p);
    }

    /// Transfer ownership of an adopted_pointer to a std::unique_ptr.
    /// @ingroup CoreFunctions
    /// @tparam is the type of object being pointed to by the adopted_ptr
    /// @tparam Deleter is the type of deleter to store in the unique_ptr.
    /// @param ap is the adopted_pointer from which ownership of the pointer will be taken.
    /// @param del is the deleter to store in the std::unique_ptr
    /// @see std::unique_ptr
    /// @post ap.empty() == true
    /// @returns std::unique_ptr<T, std::decay_t<Deleter>> which owns the pointer
    /// @note it is permissible to pass an empty adopted_pointer. This will result in the returned unique_ptr
    ///       containing a nullptr
    /// @relates adopted_pointer
    template<class T, class Deleter>
    auto to_unique(adopted_pointer<T> ap, Deleter&& del)
    {
        using result_type = std::unique_ptr<T, std::decay_t<Deleter>>;
        return result_type(std::move(ap).get(), std::forward<Deleter>(del));
    };

}}