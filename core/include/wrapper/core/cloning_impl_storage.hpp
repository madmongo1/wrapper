//
// Created by Richard Hodges on 21/07/2018.
//

#pragma once

#include <wrapper/core/base_impl_storage.hpp>

namespace wrapper { namespace core {

    /// An ImplStorage model which supports move, clone and destroy
    /// @tparam Service is the service type
    template<class Service>
    struct cloning_impl_storage
        : base_impl_storage<Service>
    {
        using base_impl_storage<Service>::base_impl_storage;
        using base_impl_storage<Service>::service;
        using base_impl_storage<Service>::impl;

        /// Support copy construction through the service
        cloning_impl_storage(cloning_impl_storage const& other)
            : base_impl_storage<Service>(copy_service(), other)
        {
            service().clone(impl(), other.impl());
        }

        /// Support copy assignment through the service
        cloning_impl_storage& operator =(cloning_impl_storage const& other)
        {
            auto tmp = cloning_impl_service(other);
            swap(tmp);
            return *this;
        }

    private:
        using base_impl_storage<Service>::copy_service;
    };

}}