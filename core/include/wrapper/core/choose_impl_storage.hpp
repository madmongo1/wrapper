//
// Created by Richard Hodges on 21/07/2018.
//

#pragma once

#include <wrapper/core/cloning_impl_storage.hpp>

namespace wrapper { namespace core {

    /// Utility to detect whether a Service concept has a compatible clone method
    template<class T, typename = void>
    struct has_clone
        : std::false_type
    {
    };

    /// Specialisaton of has_clone which is selected when Service has a correct clone() method
    template<class Service>
    struct has_clone
        <Service,
         void_t
             <
                 decltype(std::declval<Service>()
                     .clone(std::declval<typename Service::implementation_type>(),
                            std::declval<typename Service::implementation_type const>()))
             >
        >
        : std::true_type
    {
    };

    /// A template which determines the correct model of the ImplStorage concept to use as a base class
    /// for a handle object
    template<class Service> using choose_impl_storage = std::conditional_t<
        has_clone<Service>::value,
        cloning_impl_storage<Service>,
        base_impl_storage<Service>
    >;

}}
