/** @mainpage Welcome to the Wrapper Library
 *
 * @section intro_sec Introduction
 *
 * Wrapper is a utility library designed to provide c++ value objects which wrap objects delivered by c libraries
 *
 * @section install_sec Installation
 *
 * @subsection step1 Step 1: Opening the box
 *
 * @copyright (c) 2018 Richard Hodges
 *
 * @authors Richard Hodges (hodges.r@gmail.com)
 *
 */

 /**
 * @defgroup CoreFunctions Core Free Functions
 * @defgroup Core Core Classes
*/
