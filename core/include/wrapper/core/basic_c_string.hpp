//
// Created by Richard Hodges on 18/07/2018.
//

#pragma once

#include <memory>
#include <cstring>
#include <cassert>

#include <wrapper/adopt.hpp>
#include <wrapper/core/choose_impl_storage.hpp>

namespace wrapper { namespace core {




    template<class Service>
    struct basic_c_string
        : choose_impl_storage<Service>
    {
        using service_type = Service;
        using implementation_type = typename service_type::implementation_type;
        using char_type = typename service_type::char_type;
        using const_char_type = std::add_const_t<char_type>;

        using choose_impl_storage <Service>::service_args;
        using choose_impl_storage <Service>::impl_args;

        basic_c_string(adopted_pointer<char_type> ptr)
        : choose_impl_storage<Service>(service_args(), impl_args(std::move(ptr)))
        {
        }

        template<class T = char_type>
        auto data() -> std::enable_if_t<!std::is_const<T>(), char_type*>
        {
            return service().data(impl());
        }

        auto data() const -> const_char_type*
        {
            return service().data(impl());
        }

        auto c_str() const -> const_char_type*
        {
            return data();
        }

        auto operator==(basic_c_string const& r) const
        {
            return service().is_equal(impl(), r.impl());
        }

        auto operator!=(basic_c_string const& r) const
        {
            return service().not_equal(impl(), r.impl());
        }

        auto size() const -> std::size_t
        {
            return service().size(impl());
        }

        auto empty() const -> bool
        {
            return size() == 0;
        }

        auto invalid() const -> bool
        {
            return service().invalid(impl());
        }


        using choose_impl_storage<Service>::service;
        using choose_impl_storage<Service>::impl;
    };

    template<class Service>
    auto operator<<(std::ostream& os, basic_c_string<Service> const& str) -> std::ostream&
    {
        auto p = str.c_str();
        if (p)
            os << p;
        else
            os << "";
        return os;
    }

}}