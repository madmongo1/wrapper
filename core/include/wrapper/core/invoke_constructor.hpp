//
// Created by Richard Hodges on 24/07/2018.
//

#pragma once
#include <tuple>

namespace wrapper { namespace core {

    namespace detail {
        template<class T, class Tuple, std::size_t...Is>
        constexpr auto invoke_constructor(Tuple&& tuple, std::index_sequence<Is...>) -> T
        {
            return T(std::get<Is>(std::forward<Tuple>(tuple))...);
        };
    }

    template<class T, class...Args>
    auto invoke_constructor(std::tuple < Args...> const& tuple) -> T
    {
        return detail::invoke_constructor<T>(tuple, std::make_index_sequence<sizeof...(Args)>());
    };

    template<class T, class...Args>
    auto invoke_constructor(std::tuple<Args...>&& tuple) -> T
    {
        return detail::invoke_constructor<T>(std::move(tuple), std::make_index_sequence<sizeof...(Args)>());
    };
}}
