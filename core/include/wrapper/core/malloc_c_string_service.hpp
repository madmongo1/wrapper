//
// Created by Richard Hodges on 19/07/2018.
//

#pragma once
#include <wrapper/core/c_string_ops.hpp>

namespace wrapper { namespace core {

    template<class Char = char>
    struct malloc_c_string_service
        : c_string_ops<Char>
    {
        using char_type = typename c_string_ops<Char>::char_type;
        using const_char_type = typename c_string_ops<Char>::const_char_type;
        using implementation_type = typename c_string_ops<Char>::implementation_type;

        static constexpr auto acquire_service() -> malloc_c_string_service
        {
            return {};
        }

        void construct(implementation_type& impl, adopted_pointer<char_type> ptr) noexcept
        {
            impl = std::move(ptr).get();
        }

        auto clone(implementation_type& dest, implementation_type const& source) -> void
        {
            if (invalid(source))
            {
                dest = source;
            }
            else
            {
                dest = strdup(source);
            }
        }

        auto destroy(implementation_type& impl) noexcept
        {
            if (!this->invalid(impl))
                free(impl);
        }
    };

}}
