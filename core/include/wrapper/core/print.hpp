//
// Created by Richard Hodges on 26/09/2018.
//

#pragma once

#include <utility>
#include <ios>                          // streamsize
#include <boost/iostreams/categories.hpp>  // sink_tag
#include <boost/iostreams/stream.hpp>  // stream

namespace wrapper::core
{
    namespace detail
    {
        struct string_ref_sink
        {
            typedef char char_type;
            typedef boost::iostreams::sink_tag category;

            constexpr string_ref_sink(std::string& buffer) : buffer_(buffer) {}

            std::streamsize write(const char *s, std::streamsize n)
            {
                buffer_.append(s, n);
                return n;
            }

            std::string& buffer_;
        };

        inline
        auto make_stream_target(std::string& buffer) -> boost::iostreams::stream<string_ref_sink>
        {
            return boost::iostreams::stream<string_ref_sink>(buffer);
        }

        inline
        auto make_stream_target(std::ostream& stream) -> std::ostream&
        {
            return stream;
        }

        template<class T> struct reference_or_object { using type = T; };
        template<class T> struct reference_or_object<T&> { using type = T&; };
        template<class T> struct reference_or_object<T&&> { using type = T; };
        template<class T> using reference_or_object_t = typename reference_or_object<T>::type;
    }

    /// Print a sequence of items to a buffer or stream, emit a newline into the
    /// buffer or stream and return the buffer or stream or a reference to it as appropriate
    template<class Target, class...Ts>
    auto print(Target &&buffer, Ts &&...things) -> detail::reference_or_object_t<Target>
    {
        auto&& stream = detail::make_stream_target(buffer);
        ((stream << things), ...);
        stream << stream.widen('\n');
        return std::forward<Target>(buffer);
    }
}