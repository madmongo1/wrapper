#pragma once

#include <wrapper/core/void.hpp>"
#include <type_traits>


namespace wrapper { namespace core { namespace concept {

#ifdef WRAPPER_RUNNING_UNDER_DOXYGEN

    /// @addtogroup Concepts Concepts

        /// The concept of a service
        /// @ingroup Concepts
        struct Service
        {
            using implementation_type = unspecified;

            /// acquire a new or existing instance of the service
            template<class Args...>
            static decltype(auto) acquire_service(std::tuple<Args...> args);

            /// Construct a new implementation_type and store it in impl
            /// @param impl is the currently invalid implementation reference in which to store the new impl
            /// @param args are the implementation_type's constructor arguments

            void construct(implementation_type& impl, Args&&...args);

            /// Destroy an implementation.
            /// @pre !invalid(impl)
            /// @post the implementation is destroyed
            /// @post impl == invalid()
            void destroy(implementation_type& impl);

            /// Test whether an implementation has an invalid value
            /// @returns bool indicating that the implementation is invalid
            bool invalid(implementation_type const& impl);

            /// Return an implementation I for which invalid(I) == true
            /// @returns implementation_type an invalid implementation
            /// @note for an implementation which is a raw pointer, a sensible value would be nullptr
            ///
            implementation_type invalid();

            /// Move the implementation.
            /// @param dest is a reference to the destination implementation.
            /// @param source is a reference to the source implementation which will be invalidated
            /// @pre dest should not contain a reference to a valid instance as it will be overwritten
            /// @post source == invalid()
            ///
            void move(implementation_type& dest, implementation_type& source);

            /// Construct a new instance from an existing one
            /// @note this method is optional. If it exists the resulting handle type will be copyable
            /// @param dest is a reference to the destination implementation
            /// @param source is a const reference to the source implementation
            ///
            void clone(implementation_type& dest, implementation_type const& source);

        };

#endif

    template<class Service, typename = void>
    struct has_implementation_type
        : std::false_type
    {
    };

    template<class Service>
    struct has_implementation_type<
        Service,
        void_t<typename Service::implementation_type>
    >
        : std::true_type
    {
    };


    template<typename Service>
    struct is_service
        : std::integral_constant<bool,
                                 has_implementation_type<Service>::value
        >
    {
    };


}}}
