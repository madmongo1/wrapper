//
// Created by Richard Hodges on 19/07/2018.
//

#pragma once

#include <wrapper/core/adopted_pointer.hpp>

namespace wrapper {

    using ::wrapper::core::adopted_pointer;
    using ::wrapper::core::adopt;

}
